package unioeste.geral.bo.endereco;

public class Bairro {
    
    private int idBairro;
    private String nome;

    public Bairro(int idBairro, String nome) {
        this.idBairro = idBairro;
        this.nome = nome;
    }

    
    public int getIdBairro() {
        return idBairro;
    }

    public void setIdBairro(int idBairro) {
        this.idBairro = idBairro;
    }
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
}
