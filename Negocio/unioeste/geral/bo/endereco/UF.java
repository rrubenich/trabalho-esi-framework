package unioeste.geral.bo.endereco;


public class UF {
    private int idUF;
    private String nome;
    private String sigla;

    public int getIdUF() {
        return idUF;
    }

    public void setIdUF(int idUF) {
        this.idUF = idUF;
    }

    public UF(int idUF, String nome, String sigla) {
        this.idUF = idUF;
        this.nome = nome;
        this.sigla = sigla;
    }

    
    /**
     * @return the sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * @param sigla the sigla to set
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
}
