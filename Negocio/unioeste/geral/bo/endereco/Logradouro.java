package unioeste.geral.bo.endereco;


public class Logradouro {
    
    private int idLogradouro;
    private String nome;
    private TipoLogradouro tipo;

    public Logradouro(int idLogradouro, String nome, TipoLogradouro tipo) {
        this.idLogradouro = idLogradouro;
        this.nome = nome;
        this.tipo = tipo;
    }
    
    public int getIdLogradouro() {
        return idLogradouro;
    }

    public void setIdLogradouro(int idLogradouro) {
        this.idLogradouro = idLogradouro;
    }


    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the tipo
     */
    public TipoLogradouro getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoLogradouro tipo) {
        this.tipo = tipo;
    }
}
