package unioeste.geral.bo.endereco;

public class Cidade {
    
    private int idCidade;
    private String nome;
    private UF uf;

    public Cidade(int idCidade, String nome, UF uf) {
        this.idCidade = idCidade;
        this.nome = nome;
        this.uf = uf;
    }
    
    

    public int getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the uf
     */
    public UF getUf() {
        return uf;
    }

    /**
     * @param uf the uf to set
     */
    public void setUf(UF uf) {
        this.uf = uf;
    }
}
