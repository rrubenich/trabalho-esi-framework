package unioeste.geral.bo.endereco;


public class Pais {
    private int idPais;
    private String nome;
    private String sigla;

    public Pais(int idPais, String nome, String sigla) {
        this.idPais = idPais;
        this.nome = nome;
        this.sigla = sigla;
    }

    
    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    /**
     * @return the sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * @param sigla the sigla to set
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
