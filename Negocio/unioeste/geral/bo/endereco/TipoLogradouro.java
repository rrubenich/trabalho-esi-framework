package unioeste.geral.bo.endereco;


public class TipoLogradouro {
    private int idTipoLogradouro;
    private String nome;
    private String sigla;

    public TipoLogradouro(int idTipoLogradouro, String nome, String sigla) {
        this.idTipoLogradouro = idTipoLogradouro;
        this.nome = nome;
        this.sigla = sigla;
    }

    
    public int getIdTipoLogradouro() {
        return idTipoLogradouro;
    }

    public void setIdTipoLogradouro(int idTipoLogradouro) {
        this.idTipoLogradouro = idTipoLogradouro;
    }

    /**
     * @return the sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * @param sigla the sigla to set
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
}
