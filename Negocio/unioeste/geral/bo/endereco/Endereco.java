package unioeste.geral.bo.endereco;

import java.io.Serializable;
import unioeste.geral.exception.endereco.EnderecoException;

public class Endereco implements Serializable{
    private int idEndereco;
    private String cep;
    private Pais pais;
    private Bairro bairro;
    private Cidade cidade;
    private Logradouro logradouro;

    
    public Endereco(int idEndereco, String cep, Pais pais, Bairro bairro, Cidade cidade, Logradouro logradouro) {
        this.idEndereco = idEndereco;
        this.cep = cep;
        this.pais = pais;
        this.bairro = bairro;
        this.cidade = cidade;
        this.logradouro = logradouro;
    }
    
    public Endereco(){
        
    }
    
    public Endereco(int idEndereco, int idBairro, int idUF, int idCidade, 
            int idPais, int idTipoLogradouro, int idLogradouro,
            String cep, String nomePais, 
            String siglaPais, String nomeCidade, String nomeBairro, 
            String nomeUF, String siglaUF, String nomeLogradouro, 
            String nomeTipoLogradouro, String siglaTipoLogradouro){
        
        UF uf               = new UF(idUF, nomeUF, siglaUF);
        Bairro bar          = new Bairro(idBairro, nomeBairro);
        Cidade cid          = new Cidade(idCidade, nomeCidade, uf);
        Pais pai            = new Pais(idPais, nomePais, siglaPais);
        TipoLogradouro tLog = new TipoLogradouro(idTipoLogradouro, nomeTipoLogradouro, siglaTipoLogradouro);
        Logradouro log      = new Logradouro(idLogradouro, nomeLogradouro, tLog);
        
        this.idEndereco = idEndereco;
        this.cep = cep;
        this.pais = pai;
        this.bairro = bar;
        this.cidade = cid;
        this.logradouro = log;
        
    }
    /**
     * @return the cep
     */
    public String getCep() {
        return cep;
    }

    /**
     * @param cep the cep to set
     */
    public void setCep(String cep) {
        this.cep = cep;
    }

    /**
     * @return the pais
     */
    public Pais getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(Pais pais) {
        this.pais = pais;
    }

    /**
     * @return the bairro
     */
    public Bairro getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    /**
     * @return the cidade
     */
    public Cidade getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the logradouro
     */
    public Logradouro getLogradouro() {
        return logradouro;
    }

    /**
     * @param logradouro the logradouro to set
     */
    public void setLogradouro(Logradouro logradouro) {
        this.logradouro = logradouro;
    }

    /**
     * @return the idEndereco
     */
    public int getIdEndereco() {
        return idEndereco;
    }

    /**
     * @param idEndereco the idEndereco to set
     */
    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }
    
    
    public void validaCep(String cep) throws EnderecoException{
    
        if(!(cep.matches("(\\d{5}-\\d{3})"))){
            throw new EnderecoException("Formato de CEP invalido!");
        }
    }
    
    public String validaIdEndereco(String id) throws EnderecoException {
        if(id.matches("\\d*")){
            return id;
        }else{
            throw new EnderecoException("Formado de ID invalido");
        }
    }
    
    public void validaEndereco(Endereco end) throws EnderecoException{
        if(end.getCidade().getNome().length() == 0){
            throw new EnderecoException("Nome da cidade invalida");
        }
        if(end.getCidade().getUf().getNome().length() == 0){
            throw new EnderecoException("Nome do estado invalido");
        }
        if(end.getCidade().getUf().getSigla().length() == 0){
            throw new EnderecoException("Sigla do estado invalido");
        }
        if(end.getLogradouro().getNome().length() == 0){
            throw new EnderecoException("Nome do logradouro invalido");
        }
        if(end.getLogradouro().getTipo().getNome().length() == 0){
            throw new EnderecoException("Nome do tipo do logradouro invalido");
        }
        if(end.getLogradouro().getTipo().getSigla().length() == 0){
            throw new EnderecoException("Sigla do tipo do logradouro invalida");
        }
        if(end.getBairro().getNome().length() == 0){
            throw new EnderecoException("Nome do bairro invalido");
        }
        if(end.getPais().getNome().length() == 0){
            throw new EnderecoException("Nome do pais invalido");
        }
        if(end.getPais().getSigla().length() == 0){
            throw new EnderecoException("Sigla do pais invalido");
        }
    } 
}
