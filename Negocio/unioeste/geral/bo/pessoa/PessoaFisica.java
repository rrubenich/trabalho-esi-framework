/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.pessoa;

import unioeste.geral.bo.documento.*;

/**
 *
 * @author rafakx
 */
public class PessoaFisica extends Pessoa{
    private CPF numeroCPF;
    private Sexo sexo;
    private DocID numeroDocNacional;

    public CPF getNumeroCPF() {
        return numeroCPF;
    }

    public void setNumeroCPF(CPF numeroCPF) {
        this.numeroCPF = numeroCPF;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public DocID getNumeroDocNacional() {
        return numeroDocNacional;
    }

    public void setNumeroDocNacional(DocID numeroDocNacional) {
        this.numeroDocNacional = numeroDocNacional;
    }
}
