/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.pessoa;

import unioeste.geral.bo.documento.CNPJ;

/**
 *
 * @author rafakx
 */
public class PessoaJuridica extends Pessoa{
    private CNPJ numeroCNPJ;

    public CNPJ getNumeroCNPJ() {
        return numeroCNPJ;
    }

    public void setNumeroCNPJ(CNPJ numeroCNPJ) {
        this.numeroCNPJ = numeroCNPJ;
    }
}
