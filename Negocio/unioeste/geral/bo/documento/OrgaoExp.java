/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.documento;

import unioeste.geral.bo.endereco.UF;

/**
 *
 * @author rafakx
 */
public class OrgaoExp {
    private String orgaoExp;
    private UF uf;

    public String getOrgaoExp() {
        return orgaoExp;
    }

    public void setOrgaoExp(String orgaoExp) {
        this.orgaoExp = orgaoExp;
    }

    public UF getUf() {
        return uf;
    }

    public void setUf(UF uf) {
        this.uf = uf;
    }
}
