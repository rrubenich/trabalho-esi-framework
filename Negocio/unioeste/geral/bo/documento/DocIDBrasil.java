/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.bo.documento;

/**
 *
 * @author rafakx
 */
public class DocIDBrasil extends DocID{
    private String numeroRG;
    private OrgaoExp orgaoExp;

    public String getNumeroRG() {
        return numeroRG;
    }

    public void setNumeroRG(String numeroRG) {
        this.numeroRG = numeroRG;
    }

    public OrgaoExp getOrgaoExp() {
        return orgaoExp;
    }

    public void setOrgaoExp(OrgaoExp orgaoExp) {
        this.orgaoExp = orgaoExp;
    }
}
