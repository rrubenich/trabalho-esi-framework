package unioeste.teste;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import unioeste.geral.bo.endereco.*;
import unioeste.geral.manager.UCEnderecoGeralServicos;
import unioeste.geral.vo.endereco.Endereco;

public class Teste {

    private static Scanner s;

    public static int opcMenu() {
        System.out.println("1 - Cadastrar endereco");
        System.out.println("2 - Alterar endereco");
        System.out.println("3 - Excluir endereco");
        System.out.println("4 - Obter endereco por ID");
        System.out.println("5 - Obter endereco por CEP");
        System.out.println("6 - Obter endereco por Site");
        System.out.println("7 - Obter endereco por Facebook");
        System.out.println("8 - Sair");
        System.out.print("Seleciona uma opção: ");
        s = new Scanner(System.in);

        return s.nextInt();
    }

    public static void menu() {
        int opc = 0, aux = 0;
        UCEnderecoGeralServicos servGeral = new UCEnderecoGeralServicos();
        Endereco endExample = new Endereco(0, "00000-000", "Brasil", "BR", "Foz do Iguaçu", "Morumbi", "Paraná", "PR", "Tancredo Neves", "Avenida", "Av.");
        Endereco end;

        do {
            opc = opcMenu();
            System.out.println();
            switch (opc) {
                case 1:
                    try {
                        servGeral.cadastrarEndereco(pegarDadosEndereco(endExample));
                        System.out.println("Cadastrado com sucesso!");
                    } catch (EnderecoException ex) {
                        System.out.println("Erro de Formatação: " + ex.getMessage());
                    } catch (SQLException ex) {
                        System.out.println("Erro de Infra: " + ex.getMessage());
                    }
                    break;
                case 2:
                    System.out.println("Insira o ID do endereco que deseja alterar: ");
                    aux = s.nextInt();
                    try {
                        end = servGeral.obterEnderecoPorID(aux);
                        servGeral.alterarEndereco(pegarDadosEndereco(end));
                        System.out.println("Alterado com sucesso!");
                    } catch (EnderecoException ex) {
                        System.out.println("Erro de Formatação: " + ex.getMessage());
                    } catch (SQLException ex) {
                        System.out.println("Erro de Infra: " + ex.getMessage());
                    }
                    break;
                case 3:
                    System.out.println("Insira o ID do endereco que deseja excluir: ");
                    aux = s.nextInt();
                    try {
                        end = servGeral.obterEnderecoPorID(aux);
                        servGeral.excluirEndereco(end);
                        System.out.println("Excluido com sucesso!");
                    } catch (SQLException ex) {
                        System.out.println("Erro de Infra: " + ex.getMessage());
                    }
                    break;
                case 4:
                    System.out.println("Insira o ID do endereco que deseja buscar: ");
                    aux = s.nextInt();
                    try {
                        end = servGeral.obterEnderecoPorID(aux);
                        imprimeEndereco(end);
                    } catch (SQLException ex) {
                        System.out.println("Erro de Infra: " + ex.getMessage());
                    }
                    break;
                case 5:
                    System.out.println("Insira o CEP do endereco que deseja buscar: ");
                    String cep = s.next();
                    try {
                        end = servGeral.obterEnderecoPorCep(cep);
                        imprimeEndereco(end);
                    } catch (EnderecoException ex) {
                        System.out.println("Erro de Formatação: " + ex.getMessage());
                    }
                    break;
                case 6:
                    System.out.println("Insira o SITE para busca de endereco: ");
                    String site = s.next();
                    try {
                        try {
                            end = servGeral.obterEnderecoPorSite(site);
                            imprimeEndereco(end);
                        } catch (IOException ex) {
                            System.out.println("URL invalida");
                        }
                    } catch (EnderecoException ex) {
                        System.out.println("Erro de Formatação: " + ex.getMessage());
                    }
                    break;
                case 7:
                       System.out.println("Função não implementada.");
                    break;
                case 8:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção Inválida!");
                    break;
            }

            System.out.println();
            System.out.println();

        } while (opc != 8);
    }

    public static Endereco pegarDadosEndereco(Endereco e) {
        int idEndereco;
        String cep, nomePais, siglaPais, nomeCidade, nomeBairro, nomeUF, siglaUF, nomeLogradouro, nomeTipoLogradouro, siglaTipoLogradouro;

        idEndereco = e.getIdEndereco();

        String ts = s.nextLine();
        System.out.println("Insira os dados a seguir.");

        System.out.print("CEP (Ex: " + e.getCep() + "): ");
        cep = s.nextLine();

        System.out.print("Nome do País (Ex: " + e.getPais().getNome() + "): ");
        nomePais = s.nextLine();

        System.out.print("Sigla do País (Ex: " + e.getPais().getSigla() + "): ");
        siglaPais = s.nextLine();

        System.out.print("Nome da Cidade (Ex: " + e.getCidade().getNome() + "): ");
        nomeCidade = s.nextLine();

        System.out.print("Nome do Bairro (Ex: " + e.getBairro().getNome() + "): ");
        nomeBairro = s.nextLine();

        System.out.print("Nome do UF (Ex: " + e.getCidade().getUf().getNome() + "): ");
        nomeUF = s.nextLine();

        System.out.print("Sigla do UF (Ex: " + e.getCidade().getUf().getSigla() + "): ");
        siglaUF = s.nextLine();

        System.out.print("Nome do tipo do Logradouro (Ex: " + e.getLogradouro().getTipo().getNome() + "): ");
        nomeTipoLogradouro = s.nextLine();

        System.out.print("Tipo do Logradouro (Ex: " + e.getLogradouro().getTipo().getSigla() + "): ");
        siglaTipoLogradouro = s.nextLine();

        System.out.print("Nome do Logradouro (Ex: " + e.getLogradouro().getNome() + "): ");
        nomeLogradouro = s.nextLine();

        e = new Endereco(idEndereco, cep, nomePais, siglaPais, nomeCidade, nomeBairro, nomeUF,
                siglaUF, nomeLogradouro, nomeTipoLogradouro, siglaTipoLogradouro);

        return e;
    }

    public static void imprimeEndereco(Endereco e) {
        System.out.println();
        System.out.println("ID: " + e.getIdEndereco());
        System.out.println("CEP: " + e.getCep());
        System.out.println("Nome do País: " + e.getPais().getNome());
        System.out.println("Sigla do País: " + e.getPais().getSigla());
        System.out.println("Nome da Cidade: " + e.getCidade().getNome());
        System.out.println("Nome do Bairro: " + e.getBairro().getNome());
        System.out.println("Nome do UF: " + e.getCidade().getUf().getNome());
        System.out.println("Sigla do UF: " + e.getCidade().getUf().getSigla());
        System.out.println("Nome do tipo do Logradouro: " + e.getLogradouro().getTipo().getNome());
        System.out.println("Tipo do Logradouro: " + e.getLogradouro().getTipo().getSigla());
        System.out.println("Nome do Logradouro: " + e.getLogradouro().getNome());
    }

    public static void main(String[] args) {
        menu();
    }

}
