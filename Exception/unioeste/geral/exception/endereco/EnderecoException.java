package unioeste.geral.exception.endereco;

public class EnderecoException extends Exception{
    private String mensagem;
    
    public EnderecoException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
}
