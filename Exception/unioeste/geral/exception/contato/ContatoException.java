/**
 * 
 */
package unioeste.geral.exception.contato;

/**
 * @author George
 *
 */
public class ContatoException extends Exception {
    private String mensagem;
    
    public ContatoException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
    
    void insere (){
    	
    }
}
