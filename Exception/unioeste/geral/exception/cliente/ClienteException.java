/**
 * 
 */
package unioeste.geral.exception.cliente;

/**
 * @author George
 *
 */
public class ClienteException extends Exception {
    private String mensagem;
    
    public ClienteException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
}
