/**
 * 
 */
package unioeste.geral.exception.documento;

/**
 * @author George
 *
 */
public class DocumentoException extends Exception {
    private String mensagem;
    
    public DocumentoException(String mensagem){
        super(mensagem);
        this.mensagem = mensagem;
    }
    
    public String getMessage(){
        return mensagem;
    }
}
