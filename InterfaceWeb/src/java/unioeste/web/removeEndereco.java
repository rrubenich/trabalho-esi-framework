package unioeste.web;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.manager.UCEnderecoGeralServicos;
import unioeste.geral.vo.endereco.Endereco;

/**
 *
 * @author rafakx
 */
@WebServlet(urlPatterns = {"/removeEndereco"})
public class removeEndereco extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
 try (PrintWriter out = response.getWriter()) {

            if (request.getCharacterEncoding() == null) {
                request.setCharacterEncoding("UTF-8");
            }

            UCEnderecoGeralServicos UCEndereco = new UCEnderecoGeralServicos();
            Conexao.conectar();
            
            if (request.getParameter("buscaID") != null) {
                try {

                    int idEndereco = Integer.parseInt(request.getParameter("id"));
                    Endereco endereco = UCEndereco.obterEnderecoPorID(idEndereco);

                    request.setAttribute("id", endereco.getIdEndereco());
                    request.setAttribute("cep", endereco.getCep());
                    request.setAttribute("pais", endereco.getPais().getNome());
                    request.setAttribute("siglaPais", endereco.getPais().getSigla());
                    request.setAttribute("cidade", endereco.getCidade().getNome());
                    request.setAttribute("estado", endereco.getCidade().getUf().getNome());
                    request.setAttribute("siglaEstado", endereco.getCidade().getUf().getSigla());
                    request.setAttribute("siglaPais", endereco.getCidade().getUf().getSigla());
                    request.setAttribute("bairro", endereco.getBairro().getNome());
                    request.setAttribute("logradouro", endereco.getLogradouro().getNome());
                    request.setAttribute("tipoLogradouro", endereco.getLogradouro().getTipo().getNome());
                    request.setAttribute("siglaTipoLogradouro", endereco.getLogradouro().getTipo().getSigla());

                    request.setAttribute("resp", "idSucesso");

                } catch (SQLException ex) {
                    request.setAttribute("resp", "idErro");
                    request.setAttribute("erro", "Codigo inexistente");
                } catch (NumberFormatException ex) {
                    request.setAttribute("resp", "idErro");
                    request.setAttribute("erro", "Erro no formato de ID");
                }   

                RequestDispatcher dispatcher = request.getRequestDispatcher("removeEndereco.jsp");
                dispatcher.forward(request, response);
            } else {
                
                int idEndereco = Integer.parseInt(request.getParameter("id"));
                String cep = request.getParameter("cep");
                String nomePais = request.getParameter("pais");
                String siglaPais = request.getParameter("siglaPais");
                String nomeCidade = request.getParameter("cidade");
                String nomeBairro = request.getParameter("bairro");
                String nomeUF = request.getParameter("estado");
                String siglaUF = request.getParameter("siglaEstado");
                String nomeLogradouro = request.getParameter("logradouro");
                String nomeTipoLogradouro = request.getParameter("tipoLogradouro");
                String siglaTipoLogradouro = request.getParameter("siglaTipoLogradouro");

                Endereco endereco = new Endereco(idEndereco, cep, nomePais, siglaPais,
                        nomeCidade, nomeBairro, nomeUF, siglaUF, nomeLogradouro,
                        nomeTipoLogradouro, siglaTipoLogradouro);

                try {
                    UCEndereco.excluirEndereco(endereco);
                    request.setAttribute("resp", "sucesso");
                } catch (SQLException ex) {
                    request.setAttribute("resp", "sql");
                    request.setAttribute("erro", ex.getMessage());
                } catch (NumberFormatException ex) {
                    request.setAttribute("resp", "idErro");
                    request.setAttribute("erro", "Erro no formato de ID");
                }

                RequestDispatcher dispatcher = request.getRequestDispatcher("removeEndereco.jsp");
                dispatcher.forward(request, response);
            }
            Conexao.fechar();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
