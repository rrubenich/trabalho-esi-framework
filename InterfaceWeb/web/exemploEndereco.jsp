<%@ page  
    contentType="text/html; charset=UTF-8"  
%> 
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Cadastro de Endereços</title>

        <link href="bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap-3.2.0-dist/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="tema.css" rel="stylesheet">
            
    </head>

    <body>
        
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">Endereco de teste</a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="index.jsp">Cadastro</a></li>
                <li><a href="editaEndereco.jsp">Edição</a></li>
                <li><a href="removeEndereco.jsp">Remoção</a></li>
                <li><a href="buscaSiteEndereco.jsp">Busca por Site</a></li>
                <li class="active"><a href="exemploEndereco.jsp">Endereco de teste</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h2 class="page-header">Endereço de teste</h2>
            <address>Av. República Argentina, 1150 - CENTRO - Foz do Iguaçu / PR - 85109-000 - Brasil - BR</address>
        </div>
    </div>
    </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
    </body>
</html>
