package unioeste.apoio.mysql;

import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {
    public static Connection con;
    public static Statement stm;

    public static void conectar(){
        String url = "jdbc:mysql://localhost/endereco";
        String usr = "root";
        String pwd = "laieod931kais39";


        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url,usr,pwd);
            stm = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 0);	
            out.println("Conectado");
        }
        catch(SQLException ex){
            out.println("SQLEXception "+ ex.getMessage());
            out.println("SQLState "+ ex.getSQLState());
            System.exit(0);
        }
        catch(Exception e){
            out.println("Não foi possível conectar ao banco \n"+e.getMessage());
            System.exit(0);
        }
    }

    public static void fechar(){
        try {
        con.close();
        System.out.println("Conexão fechada");
        } catch (SQLException e) {
            out.println(e.getMessage());
        }
    }
}