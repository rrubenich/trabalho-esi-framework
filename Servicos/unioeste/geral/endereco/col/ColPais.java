/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.endereco.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.Pais;

/**
 *
 * @author rafakx
 */
public class ColPais {
    
    String query;
    
    public Pais consultaCodPais(Pais pais) throws SQLException{
        
        query = "SELECT idPais FROM enderecoPais WHERE nomePais = '" + pais.getNome() + "';";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        pais.setIdPais(rs.getInt("idPais"));
        
        return pais; 
    }
    
    public Pais inserePais(Pais pais) throws SQLException {
        
        query = "INSERT INTO enderecoPais (nomePais) "
              + "VALUES ('" + pais.getNome() + "')";

        Conexao.stm.executeUpdate(query);
        
        return pais;

    }
    
        
    public ResultSet resultSetPais(int codPais) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM enderecoPais WHERE idPais = '" + codPais + "' LIMIT 1;");
        rs.last();
        return rs;

    }
}
