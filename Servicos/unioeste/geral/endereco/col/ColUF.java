/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.endereco.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.UF;

/**
 *
 * @author rafakx
 */
public class ColUF {
    
    
    String query;
    
    public UF consultaCodUF(UF uf) throws SQLException{
        
        query = "SELECT idUF FROM enderecoUF WHERE nomeUF = '" + uf.getNome() + "';";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        uf.setIdUF(rs.getInt("idUF"));
        
        return uf; 
    }
    
    public UF insereUF(UF uf) throws SQLException {
        
        query = "INSERT INTO enderecoUF (nomeUF, siglaUF) "
              + "VALUES ('" + uf.getNome() + "','" + uf.getSigla() + "')";

        Conexao.stm.executeUpdate(query);
        
        return uf;

    }
    
        
    public ResultSet resultSetUF(int codUF) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM enderecoUF WHERE idUF = '" + codUF + "' LIMIT 1;");
        rs.last();
        return rs;

    }
}
