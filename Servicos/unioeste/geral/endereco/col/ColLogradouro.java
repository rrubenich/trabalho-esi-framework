/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.endereco.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.TipoLogradouro;

/**
 *
 * @author rafakx
 */
public class ColLogradouro {
    
    String query;
    
    
    public Logradouro consultaCodLogradouro(Logradouro log) throws SQLException{
        
        ColTipoLogradouro cTL = new ColTipoLogradouro();
        TipoLogradouro tLog;
        
        tLog = cTL.consultaCodTipoLogradouro(log.getTipo());

        query = "SELECT * FROM enderecoLogradouro "
                + "WHERE nomeLogradouro = '" + log.getNome() + "' "
                + "AND idTipoLogradouro = '" + tLog.getIdTipoLogradouro() + "' "
                + "LIMIT 1;";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        log.setIdLogradouro(rs.getInt("idLogradouro"));
        
        return log;
        
    }
    
    public Logradouro insereLogradouro(Logradouro log) throws SQLException{
        
        
        try{
            
            log = consultaCodLogradouro(log);
            return log;
        
        }
        catch(SQLException e){
            
            ColTipoLogradouro cTL = new ColTipoLogradouro();

            cTL.insereTipoLogradouro(log.getTipo());
            
            TipoLogradouro tLog = cTL.consultaCodTipoLogradouro(log.getTipo());
            log.setTipo(tLog);

            query = "INSERT INTO enderecoLogradouro (nomeLogradouro, idTipoLogradouro) "
                  + "VALUES ('" + log.getNome() + "','" + log.getTipo().getIdTipoLogradouro() + "')";

            Conexao.stm.executeUpdate(query);

            return log;
            
        }
    }
        
    public ResultSet resultSetLogradouro(int codLogradouro) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM enderecoLogradouro WHERE idLogradouro = '" + codLogradouro + "' LIMIT 1;");
        rs.last();
        return rs;

    }
}
