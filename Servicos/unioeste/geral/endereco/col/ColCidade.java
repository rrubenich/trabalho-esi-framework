/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unioeste.geral.endereco.col;

import java.sql.ResultSet;
import java.sql.SQLException;
import unioeste.apoio.mysql.Conexao;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.UF;

/**
 *
 * @author rafakx
 */
public class ColCidade {

    String query;
    
    
    public Cidade consultaCodCidade(Cidade cid) throws SQLException{
        
        ColUF cTL = new ColUF();
        
        UF uf;
        
        uf = cTL.consultaCodUF(cid.getUf());
        
        query = "SELECT * FROM enderecoCidade "
                + "WHERE nomeCidade = '" + cid.getNome() + "' "
                + "AND idUF = '" + uf.getIdUF() + "' "
                + "LIMIT 1;";
        
        ResultSet rs = Conexao.stm.executeQuery(query);
        rs.last();
        
        cid.setIdCidade(rs.getInt("idCidade"));
        
        return cid;
        
    }
    
    public Cidade insereCidade(Cidade cid) throws SQLException{
        
        
        try{
            
            cid = consultaCodCidade(cid);
            return cid;
        
        }
        catch(SQLException e){
            
            ColUF cTL = new ColUF();

            cTL.insereUF(cid.getUf());
            UF uf = cTL.consultaCodUF(cid.getUf());
            cid.setUf(uf);

            query = "INSERT INTO enderecoCidade (nomeCidade, idUF) "
                  + "VALUES ('" + cid.getNome() + "','" + cid.getUf().getIdUF() + "')";

            Conexao.stm.executeUpdate(query);

            return cid;
            
        }
    }
    
        
    public ResultSet resultSetCidade(int codCidade) throws SQLException {

        ResultSet rs = Conexao.stm.executeQuery("SELECT * FROM enderecoCidade WHERE idCidade = '" + codCidade + "' LIMIT 1;");
        rs.last();
        return rs;

    }
}
